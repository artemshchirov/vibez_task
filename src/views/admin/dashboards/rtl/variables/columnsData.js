export const columnsDataCheck = [
  {
    Header: "NAME",
    accessor: "name",
  },
  {
    Header: "PROGRESS",
    accessor: "progress",
  },
  {
    Header: "QUANTITY",
    accessor: "quantity",
  },
  {
    Header: "DATE",
    accessor: "date",
  },
];
export const columnsDataComplex = [
  {
    Header: "NAME",
    accessor: "name",
  },
  {
    Header: "STATUS",
    accessor: "status",
  },
  {
    Header: "DATE",
    accessor: "date",
  },
  {
    Header: "PROGRESS",
    accessor: "progress",
  },
];
export const columnsDataMembers = [
  {
    Header: "NAME",
    accessor: "person.first_name person.last_name",
  },
  {
    Header: "STATUS",
    accessor: "status",
  },
  // {
  //   Header: "MEMBER SINCE",
  //   accessor: "member_since",
  // },
  // {
  //   Header: "SOCIAL",
  //   accessor: "social",
  // },
  {
    Header: "DATE OF BIRTH",
    accessor: "dob",
  },
  {
    Header: "Gender",
    accessor: "gender",
  },
];
