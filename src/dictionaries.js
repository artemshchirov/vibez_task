export const currency_dict = {
  ILS: "₪",
  USD: "$",
  EUR: "€",
};

export const product_type_dict = {
  entry_ticket: "Entry ticket",
  child_ticket: "Child ticket",
  transport_ticket: "Transport ticket",
  merchandise: "Merchandise",
  drinks_and_food: "Drinks & Food",
  other: "Other",
};

export const bank_dict = {
  4: "בנק יהב",
  9: "בנק הדואר",
  10: "בנק לאומי",
  11: "בנק דיסקונט",
  12: "בנק הפועלים",
  13: "בנק אגוד",
  14: "בנק אוצר החייל",
  17: "בנק מרכנתיל דיסקונט",
  20: "בנק מזרחי טפחות",
  22: "Citibank N.A",
  23: "HSBC Bank plc",
  26: "יובנק",
  27: "Barclays Bank PLC",
  30: "בנק למסחר",
  31: "הבנק הבינלאומי הראשון",
  34: "בנק ערבי",
  39: "SBI State Bank of India",
  46: "בנק מסד",
  50: "מרכז סליקה בנקאי",
  52: "בנק פועלי אגודת",
  54: "בנק ירושלים",
  65: "חסך קופת חסכון לחינוך",
  68: "בנק דקסיה",
  99: "בנק ישראל",
};